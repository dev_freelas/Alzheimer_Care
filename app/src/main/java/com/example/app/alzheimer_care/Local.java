package com.example.app.alzheimer_care;

import android.graphics.Color;
import android.location.Location;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by vinic on 7/8/2015.
 */
public class Local {
    String tag;
    private String nome;
    private LatLng mLatLng;
    private long id;
    private int raio;
    private CircleOptions mCircleOptions;
    private Circle mCircle;
    private boolean isNull;

    public Marker getmMarker() {
        return mMarker;
    }

    public void setmMarker(Marker mMarker) {
        this.mMarker = mMarker;
    }

    private Marker mMarker;

    public void setmCircle(Circle mCircle) {
        this.mCircle = mCircle;
    }

    public Local(){
        isNull = true;
    }

    public Local(String tag, String nome, LatLng ll, int raio) {
        this.nome = nome;
        this.mLatLng = ll;
        this.tag = tag;
        //this.id = id;
        this.raio = raio;
        setmCircleOptions();
        isNull = false;
    }

    public Local( String tag, String nome, LatLng mLatLng, int raio, GoogleMap gmap) {
        isNull = false;
        this.mLatLng = mLatLng;
        this.nome = nome;
        this.raio = raio;
        this.tag = tag;
        setmCirclePeloMapa(gmap);
        mMarker = gmap.addMarker(new MarkerOptions()
                .position(mLatLng)
                .draggable(true)
                .title(nome));
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRaio() {
        return raio;
    }

    public void setRaio(int raio) {
        this.raio = raio;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public CircleOptions getmCircleOptions() {
        return mCircleOptions;
    }

    public void setmCircleOptions( ) {
        this.mCircleOptions = new CircleOptions()
                .center(mLatLng)
                .radius(raio)
                .strokeWidth(2)
                .strokeColor(Color.BLUE)
                .fillColor(Color.parseColor("#500084d3"));
    }

    public void setmCircleOptionsNovoRaio( int nRaio ) {
        this.mCircleOptions = new CircleOptions()
                .center(mLatLng)
                .radius(nRaio)
                .strokeWidth(2)
                .strokeColor(Color.BLUE)
                .fillColor(Color.parseColor("#500084d3"));
    }

    public LatLng getmLatLng() {
        return mLatLng;
    }

    public void setmLatLng(LatLng mLatLng) {
        this.mLatLng = mLatLng;
    }

    public Circle getmCircle() {
        return mCircle;
    }

    public void setmCirclePeloMapa(GoogleMap gmaps) {
        setmCircleOptions();
        mCircle = gmaps.addCircle(mCircleOptions);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isNull(){
        return isNull;
    }

    public Local clone(){
        Local novo = new Local(tag,nome, mLatLng, raio);
        novo.setmCircle(mCircle);
        return novo;
    }

    @Override
    public String toString() {
        return tag + ":" +
                nome + ":" +
                mLatLng.latitude + ":" +
                mLatLng.longitude + ":" +
                raio;
    }
}
