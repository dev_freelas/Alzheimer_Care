package com.example.app.alzheimer_care;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * @author Vinicis e Paulo
 * Classe que recupera contato da agenda
 */
public class ActivityContatos extends ActionBarActivity {
    //Definindo variaveis para os dados: String ? o dado e int ? codigo pra verificar se null
    String cNumber = null;
    String cName = null;

    ListView mListView;
    ArrayList<Contato> mList;
    ListaDeContatosAdapter mAdapter;

    //flag preferencia
    public static final String PREFS_NAME = "Preferences";
    SharedPreferences settings = null;

    boolean recebeuDadosDoDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_contatos);

        //para o serviço de localizaçãp
        Intent intent = new Intent(getApplicationContext(), MyService.class);
        stopService(intent);

        //instancia preferencia e seta como falso
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        settings.edit().putBoolean("mapOk", false).apply();

        //inicializa variaveis
        mList = new ArrayList<>();

        Intent recebe = getIntent();
        if(recebe.hasExtra("nome")){

            String [] nomeRecebidos = recebe.getStringArrayExtra("nome");
            String [] telRecebidos = recebe.getStringArrayExtra("tel");

            for(int i = 0; i < nomeRecebidos.length; i++){
                mList.add(new Contato(nomeRecebidos[i],telRecebidos[i]));
            }

            recebeuDadosDoDB = true;
        }else{
            recebeuDadosDoDB = false;
        }

        //instancia listview
        mListView = (ListView) findViewById(R.id.listaContatos);
        mAdapter = new ListaDeContatosAdapter(this, mList);
        mListView.setAdapter(mAdapter);
    }
    /**
     * Retorna resultados do contato obtido pelo onActivityResult e finaliza a activity
     */
    @Override
    public void finish() {


        if(!mList.isEmpty()){

            //enviar endereço para salvar no db
            String [] nomesParaSalvar = new String[mList.size()];
            int i = 0;
            for(Contato mk : mList){
                nomesParaSalvar[i] = mk.toString();
                i++;
            }
            Intent result = new Intent();
            result.putExtra("nome", nomesParaSalvar);
            result.putExtra("add", true);
            setResult(RESULT_OK, result);
        }else {
            //se nao retornar dados, tratar como configuração incompleta
            settings.edit().putBoolean("contatoOk", false).apply();

            Intent result = new Intent();
            result.putExtra("add", false);
            setResult(RESULT_OK, result);
        }
        super.finish();

    }

    public static final int PICK_CONTACT = 1; //int pra startActivityForResults->onActivityResults.int requestCod

    /**
     * Chama a intent para selecionar contato
     * @param view - Botão
     */
    public void lerDado(View view) {

        //selecionar contato
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);

    }

    /**
     * Raliza as acoes startActivityForResult
     * @param requestCode
     * @param resultCode
     * @param data - intent da agenda
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null) {
            switch (requestCode) {
                case (PICK_CONTACT):
                    if (resultCode == RESULT_OK) {


                        Uri contactData = data.getData();
                        Cursor c = managedQuery(contactData, null, null, null, null);
                        if (c.moveToFirst()) {

                            //Recuperando Dados

                            //ID
                            String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                            //Nome
                            cName = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                            //Telefone
                            String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));//verifica se h? numero
                            if (hasPhone.equalsIgnoreCase("1")) {
                                Cursor phones = getContentResolver().query(
                                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                        null, null);
                                phones.moveToFirst();
                                cNumber = null;
                                cNumber = phones.getString(phones.getColumnIndex("data1"));

                            }


                        }
                    }
                    break;
            }

            if (cNumber == null || cNumber.equalsIgnoreCase("null")){

                // 1. Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityContatos.this);
                // Create the AlertDialog

                // 2. Chain together various setter methods to set the dialog characteristics
                builder.setMessage("Selecione um contato válido")
                        .setTitle("Contato sem número");

                // Add the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();


            }else{

                Contato mm = new Contato(cName, cNumber);
                mAdapter.add(mm);
            }
        }

    }


}
