package com.example.app.alzheimer_care;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

/**
 * Main activity
 */
public class MainActivity extends ActionBarActivity {

    //flag preferencia
    public static final String PREFS_NAME = "Preferences";
    SharedPreferences settings = null;
    TextView bi;
    View lc;

    //BD
    DatabaseHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        bi = (TextView) findViewById(R.id.bttIndicador);
        lc = findViewById(R.id.colorLayout);
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        TextView pc = (TextView) findViewById(R.id.perfilCheck);
        TextView cc = (TextView) findViewById(R.id.contatoCheck);
        TextView ac = (TextView) findViewById(R.id.areaCheck);

        db = new DatabaseHelper(getApplicationContext());
        if(settings.getBoolean("perfilOk",false)){
            pc.setText("perfil Ok");
        } else pc.setText("perfil incompleto");

        if (settings.getBoolean("contatoOk",false)){
            cc.setText("contato selecionado");
        } else cc.setText("contato não selecionado");

        if (settings.getBoolean( "mapOk",false)){
            ac.setText("area informada");
        } else ac.setText("area não informada");


        //muda o botao caso Myservice ativo
        //Button btt = (Button) findViewById(R.id.bttt);
        if(isMyServiceRunning(MyService.class)){
            lc.setBackgroundColor(Color.parseColor("#FF4336"));//vermelho
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF4336")));
            bi.setText("Parar");
        }
        else {
            lc.setBackgroundColor(Color.parseColor("#4CAF50"));//vermelho
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#4CAF50")));

            bi.setText("Iniciar");
        }

    }

    /**
     * Identificca se serviço esta ativo
     * @param serviceClass - classe do serviço a ser verificada
     * @return boolean
     */
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Implementa o dialog Configuraçoes
     */
    public void doSettings(){

        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_conf);
        // Set dialog title
        dialog.setTitle(R.string.action_settings);

        // set values for custom dialog components - text, image and button
        final EditText tempAtu = (EditText) dialog.findViewById(R.id.editTempAtu);
        final EditText  tempTole = (EditText)  dialog.findViewById(R.id.editTempTole);
        final CheckBox checkBoot = (CheckBox) dialog.findViewById(R.id.checkBoot);
        dialog.show();

        if(settings.getInt("tempAtu",1919191)!=1919191 && settings.getInt("tempTole",1919191)!=1919191 ){
            System.out.println("TEmpatu1 " + settings.getInt("tempAtu",1));
            System.out.println("TEmptole1 " + settings.getInt("tempTole",1));

            tempAtu.setText(""+settings.getInt("tempAtu",1));
            tempTole.setText(""+settings.getInt("tempTole",1));
        }else {
            tempAtu.setText("10");
            tempTole.setText("2");
        }

        if (settings.getBoolean("checkBoot", false)) {
            checkBoot.setChecked(true);
        }else{
            checkBoot.setChecked(false);
        }

        checkBoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!settings.getBoolean("perfilOk", false) && !settings.getBoolean("contatoOk", false) && !settings.getBoolean("mapOk", false)) {
                    Toast.makeText(getBaseContext(), "Verifique todas as configurações antes de ativar esta opção", Toast.LENGTH_SHORT).show();
                    checkBoot.setChecked(false);
                }
            }
        });
        Button declineButton = (Button) dialog.findViewById(R.id.okBtt);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ta = tempAtu.getText().toString();
                String tt = tempTole.getText().toString();
                if (!ta.equalsIgnoreCase("") && !tt.equalsIgnoreCase("")) {
                    settings.edit().putInt("tempAtu", Integer.parseInt(ta)).commit();
                    settings.edit().putInt("tempTole", Integer.parseInt(tt)).commit();
                    System.out.println("TEmpatu2 " + settings.getInt("tempAtu", 1));
                    System.out.println("TEmptol2 " + settings.getInt("tempTole", 1));
                    if(checkBoot.isChecked()) {
                        settings.edit().putBoolean("checkBoot", true).commit();
                    }else settings.edit().putBoolean("checkBoot", false).commit();
                    // Close dialog
                    dialog.dismiss();
                } else {
                    Toast.makeText(getBaseContext(), "Valores Inválidos",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /**
     *Implementa as opções do menu
     * @param item item selecionado do menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Configuração
        if (id == R.id.action_settings) {
            //abre o dialogo de configurações
            doSettings();
        }
        //Area (mapa)
        if (id == R.id.action_area) {
            Intent intent = new Intent(this, MapsActivity.class);

            if (tagSearch("Locais")) {
                /**
                 * recupera e envia dados para activity caso exitam
                 */
                List<Todo> ttagsWatchList2 = db.getAllToDosByTag("Locais");
                String [] enderecos = new String[ttagsWatchList2.size()];
                int i = 0;
                for(Todo nn : ttagsWatchList2){
                    enderecos[i]=nn.getNote();
                    i++;
                }

                intent.putExtra("endereco", enderecos);
                db.closeDB();
            }
            startActivityForResult(intent, 12);
        }
        //Cuidadores (contatos)
        if (id == R.id.action_cuidadores) {
            Intent intent = new Intent(this, ActivityContatos.class);
            /**
             * recupera e envia dados para activity caso exitam
             */

            List<Todo> ttagsWatchList2 = db.getAllToDosByTag("vvgg");
            System.out.println("QUANTIDADE "+ttagsWatchList2.size());

            String[] telsAr = new String[ttagsWatchList2.size()];
            String[] nomesAr =new String[ttagsWatchList2.size()];

            for(int i = 0; i < ttagsWatchList2.size(); i++) {
                Todo todo2 = ttagsWatchList2.get(i);
                String[] arr2 = todo2.getNote().split(":");

                Log.i("geting", todo2.getNote());

                nomesAr[i] = arr2[0];
                telsAr[i] = arr2[1];
            }
            intent.putExtra ("nome",   nomesAr );
            intent.putExtra ("tel",   telsAr );

            db.closeDB();

            startActivityForResult(intent, 10);
        }
        //Perfil
        if (id == R.id.action_perfil) {
            Intent intent = new Intent(this, ActivityPerfil.class);

            if (tagSearch("Perfil") == true) {
                /**
                 * recupera e envia dados para activity caso exitam
                 */
                List<Todo> ttagsWatchList2 = db.getAllToDosByTag("Perfil");
                Todo todo2 = ttagsWatchList2.get(0);
                String [] arr2 = todo2.getNote().split(":");
                intent.putExtra ("nome", arr2[1] );
                intent.putExtra ("idade",arr2[2] );
                db.closeDB();
            }
            startActivityForResult(intent, 11);
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Raliza as acoes startActivityForResult
     * Em cada caso verifica se nao existe dado no BD
     * Se nado não existe, cria nova tabela e salva
     * Se existe, atualiza dados
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // super.onActivityResult(requestCode, resultCode, data);
        if(data != null) {
            switch (requestCode) {
                //Contato
                case (10): {
                    if(data.getBooleanExtra("add",false)){
                        //salvar novo
                        db.deleteTag(tagByName("vvgg"),true);
                        Tag tag = new Tag("vvgg");
                        db.createTag(tag);
                        inserir(data.getStringArrayExtra("nome"), "vvgg");
                        //setando configuração de contato feita
                        settings.edit().putBoolean("contatoOk", true).apply();

                    }else {
                        Tag teste;
                        long per;
                        if(tagSearch("vvgg")){
                            teste = tagByName("vvgg");
                            per = teste.getId();
                            System.out.println("<> ["+db.getAllToDos().get(0)+"]");
                            db.deleteTag(teste, true);
                            System.out.println("<><> ["+db.getAllToDos().get(0) + "]");
                        }
                    }
                }
                break;
                //Perfil
                case (11): {
                    if (!tagSearch("Perfil")) {
                        //Salvar nova
                        Tag perfil = new Tag("Perfil");
                        long per = db.createTag(perfil);
                        Todo todoCont = new Todo("perfil:" + data.getStringExtra("nome") + ":" + data.getStringExtra("idade"), 0);
                        long todo1_id = db.createToDo(todoCont, new long[]{per});
                        db.close();
                    }else {
                        String nova = "perfil:" + data.getStringExtra("nome") + ":" + data.getStringExtra("idade");
                        List<Todo> tagsWatchList = db.getAllToDosByTag("Perfil");
                        Todo todo = tagsWatchList.get(0);
                        String tx = todo.getNote();
                        db.deleteToDo(todo.getId());
                        Todo todo4 = new Todo(nova, 0);
                        Tag tag1 = tagByName("Perfil");
                        long per = tagId(tag1);
                        long todo3_id = db.createToDo(todo4, new long[]{per});

                        db.close();
                    }

                }
                break;
                //mapa
                case (12): {
                    if(data.hasExtra("endereco")) {

                        //salvar novo
                        inserir(data.getStringArrayExtra("endereco"),"Locais");
                        //setando configuração de contato feita
                        settings.edit().putBoolean("mapOk", true).apply();
                    }else {
                        Tag tag = tagByName("Locais");
                        db.deleteTag(tag, true);
                    }

                }
                break;
            }
        }
        db.closeDB();
    }

    public void parar(View view) {
        stopService(new Intent(this,
                MyService.class));
    }


    public void inserir( String[] array, String tagName ) {
        Tag teste;
        long per;

        if(tagSearch(tagName)){
            per = tagByName(tagName).getId();
            List<Todo> tagsWatchList = db.getAllToDosByTag(tagName);
            for(Todo todo : tagsWatchList){
                db.deleteToDo(todo.getId());
            }
            db.deleteTag(tagByName(tagName), true);

        }else{
            teste = new Tag(tagName);
            per = db.createTag(teste);
        }


        for(int i = 0; i < array.length; i++) {
            Todo todoCont = new Todo(array[i], 0);
            long todo1_id = db.createToDo(todoCont, new long[]{per});


        }
    }

    public String montarString( String[] array ){

        String resp = "";

        for(int i = 0; i < array.length; i++ ){
            if(i!=array.length-1) {
                resp = resp + array[i] + ":";
            }else{
                resp = resp + array[i];
            }
        }
        return resp;
    }

    public void deletarSelecionado( int click){
        List<Todo> tagsWatchList = db.getAllToDosByTag("Dados");
        Todo todo = tagsWatchList.get(0);
        db.deleteToDo(todo.getId());
    }

    public void mudarConfiguracoes( String[] array){
        String nova = montarString(array);
        List<Todo> tagsWatchList = db.getAllToDosByTag("Configuracoes");
        Todo todo = tagsWatchList.get(0);
        String tx = todo.getNote();
        db.deleteToDo(todo.getId());
        Todo todo4 = new Todo(nova, 0);
        Tag tag1 = tagByName("config");
        long config = tagId(tag1);
        long todo3_id = db.createToDo(todo4, new long[]{config});
        System.out.println(nova);
    }

    public boolean tagSearch(String tagName) {
        List<Tag> tags = new ArrayList<Tag>();
        tags = db.getAllTags();
        boolean resp = false;
        for (Tag tag : tags) {

            if (tag.getTagName().equalsIgnoreCase(tagName)) {
                resp = true;
            }
        }
        return resp;
    }

    public Tag clone( Tag tag ){
        Tag resp = new Tag( );
        resp.setId(tag.getId());
        resp.setTagName(tag.getTagName());
        return(resp);
    }

    /**
     * Procura uma tag pelo nome do nome
     * @param  tagName - Nome da Tag procurada
     * @return resp - Retorna a Tag procurada
     */
    public Tag tagByName(String tagName) {
        List<Tag> tags = new ArrayList<Tag>();
        tags = db.getAllTags();
        Tag tagx = new Tag();

        for (Tag tag : tags) {
            System.out.println("XXXXXX "+tag.getTagName());
            if (tag.getTagName().equalsIgnoreCase(tagName)) {
                tagx = clone(tag);
            }
        }
        return tagx;
    }

    public String getDados( long todoId){
        return db.getTodo(todoId).getNote();
    }

    public void logTodoCount( ){
        Log.e("Todo Count", "Todo count: " + db.getToDoCount());
    }

    public void logTagCount( ){
        Log.d("Tag Count", "Tag Count: " + db.getAllTags().size());
    }

    public  Tag createTag( String tagName ){
        Tag tag1 = new Tag(tagName);
        // Insere no banco de dados
        long tag1_id = db.createTag(tag1);
        return (tag1);
    }

    public long tagId( Tag tag ){
        return (tag.getId());
    }

    public long createTodo( String dado, long tag1_id){
        Todo todo1 = new Todo(dado, 0);
        long todo1_id = db.createToDo(todo1, new long[]{tag1_id});
        return todo1_id;
    }
    // Getting all tag names
    public void getAllTag( ) {
        Log.d("Get Tags", "Getting All Tags");

        List<Tag> allTags = db.getAllTags();
        for (Tag tag : allTags) {
            Log.d("Tag Name", tag.getTagName());
        }
    }

    public void getAllTodos( ) {
        // Getting all Todos
        Log.d("Get Todos", "Getting All ToDos");

        List<Todo> allToDos = db.getAllToDos();
        for (Todo todo : allToDos) {
            Log.d("ToDo", todo.getNote());
        }
    }

    public void getUnderTag( Tag tag3 ) {
        // Getting todos under "Watchlist" tag name
        Log.d("ToDo", "Get todos under single Tag name");

        List<Todo> tagsWatchList = db.getAllToDosByTag(tag3.getTagName());
        for (Todo todo : tagsWatchList) {
            Log.d("ToDo Watchlist", todo.getNote());
        }
    }

    public void deleteTodo( long todo8_id ) {
        // Deleting a   T oDo
        Log.d("Delete ToDo", "Deleting 'a Todo");
        Log.d("Tag Count", "Tag Count Before Deleting: " + db.getToDoCount());

        db.deleteToDo(todo8_id);

        Log.d("Tag Count", "Tag Count After Deleting: " + db.getToDoCount());
    }

    public void deleteAllTodo( Tag tag1 ) {
        // Deleting all Todos under "Shopping" tag
        Log.d("Tag Count",
                "Tag Count Before Deleting "+tag1.getTagName() +" Todos: "
                        + db.getToDoCount());

        db.deleteTag(tag1, true);

        Log.d("Tag Count",
                "Tag Count After Deleting "+tag1.getTagName() +" Todos:"
                        + db.getToDoCount());
    }

    public void updateTagName( Tag tag3, String newName ) {
        // Updating tag name
        tag3.setTagName(newName);
        db.updateTag(tag3);
    }

    public void closeDB( ) {
        // Don't forget to close database connection
        db.closeDB();
    }


    public void bttServico(View view) {
        if(isMyServiceRunning(MyService.class)==false) {
            if(settings.getBoolean("perfilOk",false) && settings.getBoolean("contatoOk",false ) && settings.getBoolean( "mapOk",false)){

                Intent i = new Intent(this, MyService.class);
                startService(i);
                lc.setBackgroundColor(Color.parseColor("#F44336"));//vermelho
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF4336")));

                bi.setText("Parar");

            }else Toast.makeText(getBaseContext(), "Verifique todas as configurações antes de ativar esta opção",Toast.LENGTH_SHORT).show();

        }
        else {
            Intent i = new Intent(this, MyService.class);
            stopService(i);
            lc.setBackgroundColor(Color.parseColor("#4CAF50"));//verde
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#4CAF50")));

            bi.setText("Iniciar");
        }
    }
}
