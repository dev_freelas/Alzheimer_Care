package com.example.app.alzheimer_care;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Serviso em segundo plano
 * Realiza a busca da localização e notifica os cuidadores
 */
public class MyService extends Service {
    int raioPermitido;
    int tempoAtualizacao;
    int tempoTolerancia;
    Thread t;
    Timer myTimer;
    DatabaseHelper db;
    boolean flag =true;
    public static final String PREFS_NAME = "Preferences";

    MyTask mTask;

    public MyService() {
    }

    /**
     * Recebe a string e retorna o array de acordo com padrao do banco de dados
     * @param mont
     * @return
     */
    public String[] desmontarString( String mont ){
        String[] resp =  mont.split(":");
        return resp;
    }

    int id=0;

    /**
     * Inicia o serviço
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        db = new DatabaseHelper(getApplicationContext());//iniciando db

        try {
            //recuperando local salvo
            // Ler dados do banco de dados

            List<Todo> ttagsWatchList = db.getAllToDosByTag("Locais");
            db.close();
            final List<Local> locaisSalvos = new ArrayList<>();

            for(int i = 0; i< ttagsWatchList.size(); i++){
                Todo todo = ttagsWatchList.get(i);
                String[] arr = desmontarString(todo.getNote());

                Local check = new Local(arr[0],
                        arr[1],
                        new LatLng(Double.parseDouble(arr[2]),Double.parseDouble(arr[3])),
                        Integer.parseInt(arr[4]));
                Log.i("Local recuperado", check.toString());
                locaisSalvos.add(check);

            }

            //recebe dados da configuração, caso não existam seta variaveis com valores padrao
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
            tempoAtualizacao = settings.getInt("tempAtu", 10);
            tempoTolerancia = settings.getInt("tempTole", 2);
            raioPermitido = settings.getInt("raioPermitido", 50);

            Toast.makeText(getBaseContext(), "Serviço carregado", Toast.LENGTH_SHORT).show();

            //nova thread para execução do serviço
            t = new Thread(new Runnable() {

                public void run() {
                    if(flag) {

                        //executa a cada tempoAtualizacao
                        myTimer = new Timer("MyTimer", true);
                        mTask = new MyTask(tempoTolerancia, locaisSalvos, raioPermitido, id, getApplicationContext());

                        //TEMPO DE VERIFICAÇÃO EM MILISEGUNDOS
                        myTimer.scheduleAtFixedRate(mTask, 100, (tempoAtualizacao * 60000));

                    }
                }
            });

            t.start();

            id++;
        }catch (Exception e){
            Log.e("Thread princp. servico ", e.toString());
        }
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i("MyService", "Parando serviço de localização");
        flag =false;
        mTask.cancel();
        t.interrupt();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Tarefa que sera executada a casa tempoAtualizacao
     */
    private class MyTask extends TimerTask {

        int id;
        Context context;
        List<Local> locaisSalvos;
        int raioPermitido;
        GPSLoc gpsLoc;
        int tempoTolerancia;

        /**
         * Construtor
         * @param tempoTolerancia
         * @param locaisSalvos
         * @param raioPermitido
         * @param id
         * @param mContext
         */
        public MyTask(int tempoTolerancia,List<Local> locaisSalvos, int raioPermitido, int id, Context mContext ) {
            this.id = id;
            context = mContext;
            this.locaisSalvos = locaisSalvos;
            this.raioPermitido = raioPermitido;
            this.tempoTolerancia = tempoTolerancia;
            gpsLoc = new GPSLoc(mContext);
        }

        /**
         * executa a tarefa
         */
        public void run() {
            try {

                Location localizacaoAtual = gpsLoc.getLocation();//recebe a localização

                boolean testeLocal = saiuDoLocal(locaisSalvos, localizacaoAtual); //Calcula distancia
                //se distancia atual até centro(area cadastrada) for maior que o raio cadastrado
                if (testeLocal) {

                    //espera tempoTolerancia para uma nova verificação da localização
                    Thread thread;
                    thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                synchronized (this) {
                                    wait(tempoTolerancia * 60000);//Tempo de tolerância EM MILISSEGUNDOS
                                }
                            } catch (InterruptedException ex) {
                            }

                            try {
                                GPSLoc gpsLoc2 = new GPSLoc(context);
                                Location localizacaoAtual2 = gpsLoc2.getLocation();
                                boolean testeLocal2 = saiuDoLocal(locaisSalvos, localizacaoAtual2); //Calcula distancia
                                if (testeLocal2) {

                                    //emitir alerta

                                    String nome = "";
                                    String tel = "";


                                    //Recuperar nome do paciente
                                    List<Todo> ttagsWatchList3 = db.getAllToDosByTag("Perfil");
                                    db.closeDB();

                                    Todo todo3 = ttagsWatchList3.get(0);

                                    String[] arr3 = desmontarString(todo3.getNote());
                                    String nomePaciente = "";
                                    if (arr3[0].equalsIgnoreCase("perfil")) {
                                        nome = arr3[1];
                                    }

                                    //recuperar contatos
                                    List<Todo> ttagsWatchList2 = db.getAllToDosByTag("Contato");
                                    db.closeDB();

                                    //Preparar SMS
                                    //Pegar data/hora
                                    Calendar c = Calendar.getInstance();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
                                    String currentDateandTime = sdf.format(c.getTime());

                                    //envia sms para cada um
                                    for(int i = 0; i < ttagsWatchList2.size(); i++){
                                        Todo todo2 = ttagsWatchList2.get(i);
                                        String[] arr2 = desmontarString(todo2.getNote());

                                        //envia sms
                                        SmsManager smsMgr = SmsManager.getDefault();
                                        smsMgr.sendTextMessage(
                                                arr2[1],//Nome do contato
                                                null,
                                                arr2[0] + ", " +//telefone do contato
                                                nomePaciente + " saiu da Area Limite as " +
                                                currentDateandTime + "/nUltima Posicao: " +
                                                "maps://" + localizacaoAtual2.getLongitude() + "," + localizacaoAtual2.getLatitude(),
                                                null, null);
                                    }

                                    //notificafion PROVISORIO // NAO FUNCIONA EM  2.3
                                    Notification n;
                                    NotificationManager notificationManager = (NotificationManager)
                                            getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);
                                    n = new NotificationCompat.Builder(getBaseContext())
                                            .setContentTitle("My notification")
                                            .setSmallIcon(R.mipmap.ic_launcher)
                                            .setContentText(currentDateandTime + "Lat: " + localizacaoAtual2.getLatitude() + " Long: " + localizacaoAtual2.getLongitude()).build();
                                    notificationManager.notify(id, n);
                                    id++;

                                }
                            } catch (Exception e) {
                                Log.e("Tread tempoAtu service", e.toString());
                            }
                        }
                    };
                    thread.start();

                }
            }catch (Exception ex){
                Log.e("Erro task localização", "Erro: "+ex.toString());

            }
        }

        /**
         *Calcula a distancia distancia atual com os locais salvos
         * @param locaisSalvos Lista com os locais definidos pelo usuario
         * @return se usuario saiu dos locais definidos retorna true
         */
        public boolean saiuDoLocal(List<Local> locaisSalvos, Location localizacaoAtual){
            boolean resp = true;
            for(Local nn : locaisSalvos){
                Location ll = new Location("");
                ll.setLatitude(nn.getmLatLng().latitude);
                ll.setLongitude(nn.getmLatLng().longitude);
                if(localizacaoAtual.distanceTo(ll) < nn.getRaio()){
                    Log.i("Dentro do Limite", "Local: "+nn.getNome());
                    return false;
                }
            }
            return resp;
        }

        //nao funciona
        private void sendSms(String message, String tel)
        {
            if(isSimExists())
            {

                try
                {

                    String SENT = "SMS_SENT";

                    PendingIntent sentPI = PendingIntent.getBroadcast(getBaseContext(), 0, new Intent(SENT), 0);

                    registerReceiver(new BroadcastReceiver()
                    {
                        @Override
                        public void onReceive(Context arg0, Intent arg1)
                        {
                            int resultCode = getResultCode();
                            switch (resultCode)
                            {
                                case Activity.RESULT_OK:                      Toast.makeText(getBaseContext(), "SMS sent",Toast.LENGTH_LONG).show();
                                    break;
                                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:  Toast.makeText(getBaseContext(), "Generic failure",Toast.LENGTH_LONG).show();
                                    break;
                                case SmsManager.RESULT_ERROR_NO_SERVICE:       Toast.makeText(getBaseContext(), "No service",Toast.LENGTH_LONG).show();
                                    break;
                                case SmsManager.RESULT_ERROR_NULL_PDU:         Toast.makeText(getBaseContext(), "Null PDU",Toast.LENGTH_LONG).show();
                                    break;
                                case SmsManager.RESULT_ERROR_RADIO_OFF:        Toast.makeText(getBaseContext(), "Radio off",Toast.LENGTH_LONG).show();
                                    break;
                            }
                        }
                    }, new IntentFilter(SENT));

                    SmsManager smsMgr = SmsManager.getDefault();
                    smsMgr.sendTextMessage(tel, null, message, sentPI, null);

                }
                catch (Exception e)
                {
                    Toast.makeText(getBaseContext(), e.getMessage()+"!\n"+"Falha ao enviar SMS", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            else
            {
                Toast.makeText(getBaseContext(), "Impossivel enviar SMS", Toast.LENGTH_LONG).show();
            }
        }
        //nao testado
        public boolean isSimExists()
        {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            int SIM_STATE = telephonyManager.getSimState();

            if(SIM_STATE == TelephonyManager.SIM_STATE_READY)
                return true;
            else
            {
                switch(SIM_STATE)
                {
                    case TelephonyManager.SIM_STATE_ABSENT: //SimState = "No Sim Found!";
                        break;
                    case TelephonyManager.SIM_STATE_NETWORK_LOCKED: //SimState = "Network Locked!";
                        break;
                    case TelephonyManager.SIM_STATE_PIN_REQUIRED: //SimState = "PIN Required to access SIM!";
                        break;
                    case TelephonyManager.SIM_STATE_PUK_REQUIRED: //SimState = "PUK Required to access SIM!"; // Personal Unblocking Code
                        break;
                    case TelephonyManager.SIM_STATE_UNKNOWN: //SimState = "Unknown SIM State!";
                        break;
                }
                return false;
            }
        }




    }
}

