package com.example.app.alzheimer_care;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.SeekBar;
import android.widget.TextView;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;

/**
 * Created by vinic on 7/8/2015.
 */
public class ListaDeLocaisAdapter extends BaseAdapter implements ListAdapter {

    private ArrayList<Local> list = new ArrayList<Local>();
    Context context;
    GoogleMap mGoogleMap;

    public  ListaDeLocaisAdapter(ArrayList<Local> list, Context context, GoogleMap gmap) {
        this.list = list;
        this.context = context;
        this.mGoogleMap = gmap;
    }

    public void add(Local ll){
        list.add(ll);
        notifyDataSetChanged();
    }

    public void clear(){
        list.clear();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pp = position;
        View view = convertView;
        final Local nn = list.get(position);

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.mylist, null);
        }

        //Manipula o TextView e mostra a string da item da lista
       final TextView nome = (TextView)view.findViewById(R.id.textLocalNomeLista);
        nome.setText(nn.getNome());

        final TextView tag = (TextView)view.findViewById(R.id.textListaTag);
        tag.setText(nn.getTag());

        final TextView textRaio = (TextView)view.findViewById(R.id.textRaioLista);
        textRaio.setText("" + nn.getRaio()+"m");

        SeekBar mSeekBar = (SeekBar) view.findViewById(R.id.seekBarLista);
        mSeekBar.setProgress(nn.getRaio());
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                nn.getmCircle().setRadius(progresValue);
                textRaio.setText("" + progresValue + "m");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                nn.setRaio(seekBar.getProgress());
            }

        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //move a camera ao clicar no item
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(nn.getmLatLng(), 16));
            }
        });

        //Botao de Remover
        ImageButton delete = (ImageButton) view.findViewById(R.id.buttonDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //remove da lista
                nn.getmCircle().remove();
                nn.getmMarker().remove();
                list.remove(pp);
                //atualiza ListView
                notifyDataSetChanged();
            }
        });

        ImageButton edit = (ImageButton) view.findViewById(R.id.buttonEdit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String [] param = {nn.getTag(),nn.getNome(),""+nn.getmLatLng().latitude,""+nn.getmLatLng().longitude};
                abreDialogoEditar(nn, nome, tag);
            }
        });

        return view;
    }

    public void abreDialogoEditar(final Local nn, TextView nnome, TextView ntag )
    {

        final Dialog dialog = new Dialog(context);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_local);
        // Set dialog title
        dialog.setTitle("Adicionar Local");

        // set values for custom dialog components - text, image and button
        final EditText editTagName = (EditText) dialog.findViewById(R.id.editTextDialogTag);
        final EditText  textNome = (EditText)  dialog.findViewById(R.id.textDialogNome);
        final TextView  textLat = (TextView)  dialog.findViewById(R.id.textDialogLat);
        final TextView  textLng = (TextView)  dialog.findViewById(R.id.textDialogLgn);

        editTagName.setText(nn.getTag());
        textNome.setText(nn.getNome());
        textLat.setText(""+nn.getmLatLng().latitude);
        textLng.setText(""+nn.getmLatLng().longitude);

        dialog.show();

        Button okButton = (Button) dialog.findViewById(R.id.buttonDialogOk);
        // if decline button is clicked, close the custom dialog
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nn.setNome(textNome.getText().toString());
                nn.setTag(editTagName.getText().toString());
                dialog.dismiss();
                notifyDataSetChanged();

            }
        });

        nnome.setText(textNome.getText().toString());
        ntag.setText(editTagName.getText().toString());

    }


}
