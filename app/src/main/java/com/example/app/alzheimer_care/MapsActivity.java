package com.example.app.alzheimer_care;

import com.google.android.gms.maps.model.Circle;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import android.support.v7.widget.SearchView;

public class MapsActivity extends ActionBarActivity implements OnMapReadyCallback {

    LatLng latLng;
    Local searchLocal;
    MarkerOptions markerOptions;
    Marker searchMarker;
    GoogleMap googleMap;
    String endereco="";
    Boolean pesquisado = false;
    String tagName;

    boolean recebeuDadosDoDB;

    SharedPreferences settings;
    public static final String PREFS_NAME = "Preferences";

    ListView mListView;
    ArrayList <Local> mList; //lista principal
    ListaDeLocaisAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //para o serviço de localizaçãp
        Intent intent = new Intent(getApplicationContext(), MyService.class);
        stopService(intent);

        //instancia preferencia e seta como falso
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        settings.edit().putBoolean("mapOk", false).apply();

        //inicializa variaveis
        googleMap = mapFragment.getMap();
        mList = new ArrayList<>();

        Intent recebe = getIntent();
        if(recebe.hasExtra("endereco")){

            String [] enderecosRecebidos = recebe.getStringArrayExtra("endereco");

            for(int i = 0; i<enderecosRecebidos.length; i++){
                String [] split = enderecosRecebidos[i].split(":");
                mList.add(new Local(split[0],
                        split[1],
                        new LatLng(Double.parseDouble(split[2]),Double.parseDouble(split[3]) ),
                        Integer.parseInt(split[4]),
                        googleMap ));
            }

            recebeuDadosDoDB = true;
        }else{
            recebeuDadosDoDB = false;
        }

        //instancia listview
        mListView = (ListView) findViewById(R.id.listView);
        mAdapter = new ListaDeLocaisAdapter(mList, this, googleMap);
        mListView.setAdapter(mAdapter);

    }

    @Override
    public void onMapReady(GoogleMap map) {

        //prepara mapa
        map.setMyLocationEnabled(true);
        if(recebeuDadosDoDB){
            preparaMapa();
        }else
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(0,0), 0));

        //instancia OnMarkerDragListener
        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                if (!mList.isEmpty()) {
                    for (Local m : mList) {
                        if (marker.getTitle().equalsIgnoreCase(m.getNome())) {
                            m.setmLatLng(marker.getPosition());
                            m.getmCircle().setCenter(marker.getPosition());
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        });

    }


    /**
     * Adiciona os elementos no mapa
     */
    public void preparaMapa() {
        //Todo criar metodo para ler do DB e colocar na lista
        double mediaLat = 0;
        double mediaLng = 0;
        for (Local mloc : mList) {
            mediaLat += mloc.getmLatLng().latitude;
            mediaLng += mloc.getmLatLng().longitude;
        }
        mediaLat = mediaLat/mList.size();
        mediaLng = mediaLng/mList.size();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mediaLat,mediaLng), 15));

    }


    /**
     * Botão adicionar/editar item pesquisado
     *
     */
    public void addLocal(View view) {
        if(pesquisado) {

            //
            final String [] parseEndereco = endereco.split(":");

            final Dialog dialog = new Dialog(this);
            // Include dialog.xml file
            dialog.setContentView(R.layout.dialog_local);
            // Set dialog title
            dialog.setTitle("Adicionar Local");

            // set values for custom dialog components - text, image and button
            final EditText editTagName = (EditText) dialog.findViewById(R.id.editTextDialogTag);
            final EditText  textNome = (EditText)  dialog.findViewById(R.id.textDialogNome);
            final TextView  textLat = (TextView)  dialog.findViewById(R.id.textDialogLat);
            final TextView  textLng = (TextView)  dialog.findViewById(R.id.textDialogLgn);

            textNome.setText(parseEndereco[0]);
            textLat.setText(""+searchMarker.getPosition().latitude);
            textLng.setText(""+searchMarker.getPosition().longitude);

            dialog.show();

            Button okButton = (Button) dialog.findViewById(R.id.buttonDialogOk);
            // if decline button is clicked, close the custom dialog
            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tagName = editTagName.getText().toString();
                    endereco = textNome.getText().toString();
                    endereco = tagName+":"+endereco;
                    searchLocal = new Local(tagName, parseEndereco[0], searchMarker.getPosition() , 100, googleMap);


                    mAdapter.add(searchLocal);
                    mAdapter.notifyDataSetChanged();

                    dialog.dismiss();

                    //remove marker da pesquisa
                    searchMarker.remove();
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        //Carrega o arquivo de menu.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_maps, menu);

        MenuItem searchItem = menu.findItem(R.id.search);

        //Pega o Componente.
        SearchView mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        // SearchView mSearchView = (SearchView) menu.findItem(R.id.search)       .getActionView(searchItem); PARA API +
        //Define um texto de ajuda:
        mSearchView.setQueryHint("Busque o Local");

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String arg0) {
                if(searchMarker != null)
                    searchMarker.remove();

                if (arg0 != null && !arg0.equals("")) {
                    new GeocoderTask().execute(arg0);
                }
                pesquisado = true;
                MapsActivity.this.findViewById(R.id.buttonAdd).setEnabled(true);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {

                return false;
            }

        });

        return true;
    }



    @Override
    public void finish() {
        if(!mList.isEmpty()){

            //enviar endereço para salvar no db
            String [] enderecosParaSalvar = new String[mList.size()];
            int i = 0;
            for(Local mk : mList){
                enderecosParaSalvar[i] = mk.toString();
                i++;
            }
            Intent result = new Intent();
            result.putExtra("endereco", enderecosParaSalvar);
            setResult(RESULT_OK, result);
        }else
            //se nao retornar dados, tratar como configuração incompleta
            settings.edit().putBoolean("mapOk", false).apply();
        super.finish();
    }

    //----------------------------------------------------------------------------------------------
    // An AsyncTask class for accessing the GeoCoding Web Service
    private class GeocoderTask extends AsyncTask<String, Void, List<Address>> {


        ProgressDialog dialog;


        @Override
        protected List<Address> doInBackground(String... locationName) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null;

            try {
                // Getting a maximum of 3 Address that matches the input text
                addresses = geocoder.getFromLocationName(locationName[0], 3);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Erro obter pesquisa", e.toString());
            }
            return addresses;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(MapsActivity.this, "Aguarde",
                    "Pesquisando Local...");
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {

            if(addresses==null || addresses.size()==0 || addresses.isEmpty()){
                Toast.makeText(getBaseContext(), "No Location found", Toast.LENGTH_SHORT).show();
            }

            // Adding Markers on Google Map for each matching address
            for(int i=0;i<addresses.size();i++){

                Address address = addresses.get(i);

                // Creating an instance of GeoPoint, to display in Google Map
                latLng = new LatLng(address.getLatitude(), address.getLongitude());

                String addressText = String.format("%s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        address.getCountryName());

                //grava novo local
                endereco = addressText+":"+address.getLatitude()+":"+address.getLongitude();

                //mostrar marcador
                markerOptions = new MarkerOptions().position(latLng)
                        .title(addressText);

                searchMarker = googleMap.addMarker(markerOptions);
                searchMarker.setDraggable(true);

                TextView textEnd = (TextView) MapsActivity.this.findViewById(R.id.textEndereco);
                textEnd.setText(addressText);

                // Locate the first location
                if(i==0)
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
                dialog.dismiss();
            }
        }
    }



}




